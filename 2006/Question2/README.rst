This is the attempt at the 2006 BIO Question 2.

The **test*.dat** are the test data from the **bio06-marks.pdf** paper.
The data has the form required by the question with two extra lines containing
the expected results.

Notes
=====

This is the steps (and missteps) I took to solve the problem.

0. Collect all test cases and create **test??.dat** containing::

       xddu     # the rule to use
       8654     # test password 1
       2102     # test password 2
       No       # correct answer for password 1
       Yes      # correct answer for password 2

   Also wrote a test harness **test.py** to read one or more **test??.dat**
   file, call the rule code in **rules.py** and check the results.

1. Running all the tests shows errors::

      $ ./test.py *.dat
      ***** file test06.dat, rule=xd*u?, password 54322 should get 'No', got 'Yes'
      ***** file test07.dat, rule=x?x, password 1 should get 'Yes', got 'No'
      ***** file test10.dat, rule=x(xud)?, password 7890 should get 'Yes', got 'No'
      ***** file test11.dat, rule=x(xd)?(xu)?, password 421 should get 'Yes', got 'No'
      ***** file test12.dat, rule=x(ud)*u?, password 0836 should get 'Yes', got 'No'
      ***** file test12.dat, rule=x(ud)*u?, password 19181716151 should get 'Yes', got 'No'

   We don't expect to be able to handle any cases with brackets since we haven't
   implemented that yet.  So errors for 10, 11 and 12 are expected.

   The failure on case 6 is probably due to not handling a FAIL correctly.  We
   need to handle a FAIL on an *optional* character by skipping to the next
   rule character and trying the password character again.  We aren't doing that
   hence the error.

   Test 7 looks particularly bad since we need to do **backtracking** to get the
   correct answer?!

   This version is saved as **rule_1.py**.

2. Implement recursive version to make **test07.dat** pass correctly.

   If we extend the **check()** function to take *r_index*, *p_index* and
   *prev_char* parameters we can now loop recursively so we no longer need the 
   *while* loop.  We can also handle the case when a password of "1" fails
   according to the rule "x?x".  We do this when a number matches on an optional
   "x?", "u?" or "d?".  In this case we return "Yes" if either the original
   match or a match if we skip the optional character.  We do this by returning
   the **any()** combination of the "match" and "skip" cases.  For this to work
   the function must return a True or False.  We convert to "Yes" or "No"
   outside the function.  We now pass test 7::

        $ python3 test.py test*.dat
        ***** file test10.dat, rule_str='x(xud)?', password '7890' should get 'Yes', got 'No'
        ***** file test11.dat, rule_str='x(xd)?(xu)?', password '421' should get 'Yes', got 'No'
        ***** file test12.dat, rule_str='x(ud)*u?', password '0836' should get 'Yes', got 'No'
        ***** file test12.dat, rule_str='x(ud)*u?', password '19181716151' should get 'Yes', got 'No'

    This version is saved as **rule_2.py**.
