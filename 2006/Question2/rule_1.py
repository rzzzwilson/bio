"""
Code to implement the "password checker" in the BIO 2006 Question 2.
"""

# make a version: check(rule, password, r_index=0, p_index=0)
# that allows recursive calls for backtracking using
#     "any(check(...), check(...))"

def check(rule, password):
    """Rule check function.

    rule      the rule string
    password  the password to check

    Returns "Yes" or "No" if password matches rule or not.
    """

    def check_match_trail():
        nonlocal r_index

        if r_index < len(rule):
            if rule[r_index] == '?':
                r_index += 1
            elif rule[r_index] == '*':
                r_index -= 1

    def check_fail_trail():
        """Returns True if should abort."""

        nonlocal r_index

        if r_index < len(rule) - 1:
            if rule[r_index+1] in '?*':
                r_index += 2
            if r_index == len(rule):
                return True
        else:
            return True

        return False


    r_index = 0
    prev_char = None

    for p_char in password:
        if r_index >= len(rule):
            return 'No'

        if rule[r_index] == 'x':
            r_index += 1
            check_match_trail()
            prev_char = p_char
        elif rule[r_index] == 'u':
            if not int(p_char) > int(prev_char):
                # rule fails, got trailing "?*"?
                if check_fail_trail():
                    return 'No'
                continue
            # match, decide what to do with any trailing ? or *
            r_index += 1
            check_match_trail()
            prev_char = p_char
        elif rule[r_index] == 'd':
            if not int(p_char) < int(prev_char):
                # rule fails, got trailing "?*"?
                if check_fail_trail():
                    return 'No'
                continue
            # match, decide what to do with any trailing ? or *
            r_index += 1
            check_match_trail()
            prev_char = p_char

    if r_index == len(rule) - 2:
        if rule[r_index+1] not in "*?":
            return 'No'
    elif r_index != len(rule):
        return 'No'
    return 'Yes'
