"""
Code to implement the "password checker" in the BIO 2006 Question 2.
"""

debug = False

def check(rule, password, r_index=0, p_index=0, prev_char=None):
    """Rule check function.

    rule       the rule string
    password   the password to check
    r_index    index into "rule" for current rule char
    p_index    index into "password" for current password char
    prev_char  the previous char

    Returns True if password matches rule.
    """

    def next_is_repeat():
        """Returns True if next R char is "?" or "*"."""

        if r_index + 1 >= len(rule):
            return False        # no next char, so False
        return rule[r_index+1] in '?*'

    if debug:
        print(f'check: password={password}, p_index={p_index}, rule={rule}, r_index={r_index}, prev_char={prev_char}')

    if p_index >= len(password):
        if r_index >= len(rule):
            return True
        if (r_index == len(rule)-2) and (rule[-1] in '*?'):
            return True
        return False

    if r_index >= len(rule):
        return False

    p_char = password[p_index]

    if rule[r_index] == 'x':
        if debug:
            print('got X')
        # always matches, check if repeated
        if next_is_repeat():
            if debug:
                print('next_is_repeat')
            # try with and without match
            if rule[r_index+1] == '*':
                return any((check(rule, password, r_index, p_index+1, prev_char=p_char),    # match and stay here in rule
                            check(rule, password, r_index+2, p_index, prev_char=prev_char)  # don't match and try after
                          ))
            else:
                return any((check(rule, password, r_index+2, p_index+1, prev_char=p_char),  # match and move on
                            check(rule, password, r_index+2, p_index, prev_char=prev_char)  # don't match, try after
                          ))
        return check(rule, password, r_index+1, p_index+1, prev_char=p_char)            # match and move on
    elif rule[r_index] == 'u':
        if debug:
            print('got U')
        if not int(p_char) > int(prev_char):
            # fail, see if repeated, not fatal
            if next_is_repeat():
                return check(rule, password, r_index+2, p_index, prev_char=p_char)      # don't match, try after
            return False    # no match and not optional, FAIL
        # match. if repeated, try also without match
        if next_is_repeat():
            if rule[r_index+1] == '*':
                return any((check(rule, password, r_index, p_index+1, prev_char=p_char),  # match and stay here in rule
                            check(rule, password, r_index+2, p_index, prev_char=prev_char)  # don't match and try after
                          ))
            else:
                return any((check(rule, password, r_index+2, p_index+1, prev_char=p_char),  # match and move on
                            check(rule, password, r_index+2, p_index, prev_char=prev_char)  # don't match, try after
                          ))
        return check(rule, password, r_index+1, p_index+1, prev_char=p_char)            # match and move on
    elif rule[r_index] == 'd':
        if debug:
            print('got D')
        if not int(p_char) < int(prev_char):
            # fail, see if repeated, not fatal
            if next_is_repeat():
                return check(rule, password, r_index+2, p_index, prev_char=prev_char)   # don't match, try after
            return False
        # match. if repeated, try also without match
        if next_is_repeat():
            return any((check(rule, password, r_index+2, p_index+1, prev_char=p_char),  # match and move on
                        check(rule, password, r_index+2, p_index, prev_char=prev_char)  # don't match, try after
                      ))
        return check(rule, password, r_index+1, p_index+1, prev_char=p_char)            # match and move on
