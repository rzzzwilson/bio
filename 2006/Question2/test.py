#!/usr//bin/env python3

"""
Program to take a test data file and check the results.

Usage: test.py <datafilename>
"""

import sys
import getopt
import traceback
import rule

# the main code goes here
def main(debug, file_list):
    """Read data from "file_list" and check the rule() function."""

    errors = False
    rule.debug = debug

    sorted(file_list)

    for fname in file_list:
        # gather data from the file
        passwords = []
        expected = []

        with open(fname) as fd:
            rule_str = fd.readline().strip()
            passwords.append(fd.readline().strip())
            passwords.append(fd.readline().strip())
            expected.append(fd.readline().strip())
            expected.append(fd.readline().strip())

        # now call the rule() function and check results
        for (p, e) in zip(passwords, expected):
            result = 'Yes' if rule.check(rule_str, p) else 'No'
            if result != e:
                errors = True
                print(f"***** file {fname}, rule_str='{rule_str}', password '{p}' should get '{e}', got '{result}'")

    return 1 if errors else 0

# to help the befuddled user
def usage(msg=None):
    if msg:
        print(('*'*80 + '\n%s\n' + '*'*80) % msg)
    print(__doc__)

# our own handler for uncaught exceptions
def excepthook(type, value, tb):
    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tb))
    msg += '=' * 80 + '\n'
    print(msg)

# plug our handler into the python system
sys.excepthook = excepthook

# parse the CLI params
argv = sys.argv[1:]

try:
    (opts, args) = getopt.getopt(argv, 'dh', ['debug', 'help'])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

debug = False

for (opt, param) in opts:
    if opt in ['-h', '--help']:
        usage()
        sys.exit(0)
    elif opt in ['-d', '--debug']:
        debug = True

if len(args) < 1:
    usage()
    sys.exit(1)

# run the program code
result = main(debug, args)
sys.exit(result)
